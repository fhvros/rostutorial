#!/usr/bin/env python
from __future__ import print_function

import cv2
import numpy as np

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


class cv_demo(object):
    def __init__(self):
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("/camera/rgb/image_rect_color", Image, self.callback)
        self.image_pub = rospy.Publisher("/rosball/cvdemo", Image, queue_size=10)
        self.mask_old = None

    def callback(self, data):
        try:
            self.cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            self.detection(self.cv_image)
        except CvBridgeError as e:
            print(e)

    def detection(self, image):
        # define the lower and upper boundaries of the "green"
        # ball in the HSV color space, then initialize the
        # list of tracked points
        yellowLower = (28, 86, 6)  #29 86 6
        yellowUpper = (64, 255, 255) #64 255 255

        blurred = cv2.GaussianBlur(image, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

        # construct a mask for the color "green", then perform
        # a series of dilations and erosions to remove any small
        # blobs left in the mask
        mask = cv2.inRange(hsv, yellowLower, yellowUpper)
        kernel = np.ones((1, 1), np.uint8) #original 1
        # dilate before erode with different iteration sizes
        mask = cv2.dilate(mask, kernel, iterations=5) # 4 works
        mask = cv2.erode(mask, kernel, iterations=2) #2 works, although higher then dilation seems better
        res = cv2.bitwise_and(image, image, mask=mask)

        # find contours in the mask and initialize the current (x, y) center of the ball
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

        cnts.sort(key=cv2.contourArea, reverse=True)

        # only proceed if at least one contour was found
        while len(cnts) != 0:
            ((x, y), radius) = cv2.minEnclosingCircle(cnts[0])
            del cnts[0]
            if radius > 15:
                cv2.circle(image, (int(x), int(y)), int(radius), (0, 255, 255), 2)


        img = self.bridge.cv2_to_imgmsg(res, "bgr8")
        self.image_pub.publish(img)


def main():
    rospy.init_node('cvdemo', anonymous=False)
    ic = cv_demo()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
        cv2.destroyAllWindows()

if __name__ == '__main__':
    main()